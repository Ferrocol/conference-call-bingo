$(document).ready(function() {
    $('.box').mousedown(function() {
        $(this).css({
            'border-style':'inset'
        });
    }).mouseup(function() {
        $(this).css({
            'border-style':'outset'
        });
    }).mouseout(function() {
        $(this).css({
            'border-style':'outset'
        });
    }).click(function() {
        $(this).toggleClass('selected');
        isBingo();
    });
})

function isBingo() {
        var rows = [1, 6, 11, 16, 21];
        for (var i = 0; i < rows.length; i++) {
            if ($('#' + rows[i]).hasClass('selected')
            && $('#' + (rows[i] + 1)).hasClass('selected')
            && $('#' + (rows[i] + 2)).hasClass('selected')
            && $('#' + (rows[i] + 3)).hasClass('selected')
            && $('#' + (rows[i] + 4)).hasClass('selected')) {
                alert('BINGO!');
            }
        };
        var columns = [1, 2, 3, 4, 5];
        for (var i = 0; i < columns.length; i++) {
            if ($('#' + columns[i]).hasClass('selected')
            && $('#' + (columns[i] + 5)).hasClass('selected')
            && $('#' + (columns[i] + 10)).hasClass('selected')
            && $('#' + (columns[i] + 15)).hasClass('selected')
            && $('#' + (columns[i] + 20)).hasClass('selected')) {
                alert('BINGO!');
            }
        };
        var diagonals = [1, 5];
        for (var i = 0; i < diagonals.length; i++) {
            var space;
            if (diagonals[i] == 1) {
                if ($('#' + diagonals[i]).hasClass('selected')
                && $('#' + (diagonals[i] + 6)).hasClass('selected')
                && $('#' + (diagonals[i] + 12)).hasClass('selected')
                && $('#' + (diagonals[i] + 18)).hasClass('selected')
                && $('#' + (diagonals[i] + 24)).hasClass('selected')) {
                    alert('BINGO!');
                }
            } else if (diagonals[i] == 5) {
                if ($('#' + diagonals[i]).hasClass('selected')
                && $('#' + (diagonals[i] + 4)).hasClass('selected')
                && $('#' + (diagonals[i] + 8)).hasClass('selected')
                && $('#' + (diagonals[i] + 12)).hasClass('selected')
                && $('#' + (diagonals[i] + 16)).hasClass('selected')) {
                    alert('BINGO!');
                }
            }
        }
}
